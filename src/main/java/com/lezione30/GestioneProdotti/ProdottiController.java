package com.lezione30.GestioneProdotti;

import java.sql.SQLException;
import java.util.ArrayList;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.lezione30.GestioneProdotti.model.Prodotto;
import com.lezione30.GestioneProdotti.services.ProdottoDAO;

@RestController
public class ProdottiController {

	@GetMapping("/prodotto/{id}")
	public Prodotto ricercaProdottoPerId(@PathVariable Integer id) {
		
		ProdottoDAO proDao = new ProdottoDAO();
		
		Prodotto temp = null;
		
		try {
			temp = proDao.getById(id);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		
		return temp;
	}
	
	@GetMapping("/prodotto")
	public ArrayList<Prodotto> ricercaTuttiProdotti(){
		ProdottoDAO proDao = new ProdottoDAO();
		
		ArrayList<Prodotto> elenco = new ArrayList<Prodotto>();
		
		try {
			elenco = proDao.getAll();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		
		return elenco;
	}

	@DeleteMapping("/prodotto/{id}")
	public boolean eliminaProdotto(@PathVariable Integer id) {
		ProdottoDAO proDao = new ProdottoDAO();

		try {
			Prodotto temp = proDao.getById(id);
			if(temp != null) {
				return proDao.delete(temp);
			}
			
			return false;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
		}
	}
	
	@PostMapping("/prodotto/nuovo")
	public Prodotto inserisciProdotto(@RequestBody Prodotto objProd) {
		ProdottoDAO proDao = new ProdottoDAO();

		try {
			proDao.insert(objProd);
			return objProd;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			return null;
		}
		
	}
	
	@PutMapping("/prodotto/modifica")
	public Prodotto modificaProdotto(@RequestBody Prodotto objProd) {
		ProdottoDAO proDao = new ProdottoDAO();

		try {
			Prodotto temp = proDao.getById(objProd.getProdottoid());
			temp.setCodice(objProd.getCodice());
			temp.setNome(objProd.getNome());
			temp.setDescrizione(objProd.getDescrizione());
			
			return proDao.update(temp);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}
	
	}
	
	
//	@GetMapping("/dammiprodotto")
//	public Prodotto restitutisciProdotto() {
//		
//		Prodotto temp = new Prodotto();
//		temp.setProdottoid(889);
//		temp.setCodice("MAR1234");
//		temp.setNome("Marmellata");
//		temp.setDescrizione("Descrizione della Marmellata");
//		
//		return temp;
//		
//	}
//	
//	@GetMapping("/dammielencoprodotti")
//	public ArrayList<Prodotto> restitutisciElencoProdotti() {
//		
//		ArrayList<Prodotto> elenco = new ArrayList<Prodotto>();
//		
//		Prodotto temp1 = new Prodotto();
//		temp1.setProdottoid(889);
//		temp1.setCodice("MAR1234");
//		temp1.setNome("Marmellata");
//		temp1.setDescrizione("Descrizione della Marmellata");
//		elenco.add(temp1);
//		
//		Prodotto temp2 = new Prodotto();
//		temp2.setProdottoid(889);
//		temp2.setCodice("TER1234");
//		temp2.setNome("Terra");
//		temp2.setDescrizione("Descrizione della Terra");
//		elenco.add(temp2);
//		
//		return elenco;
//		
//	}
//	
//	@PostMapping("/dammielencoprodotti")
//	public void restitutisciElencoProdottiPost() {
//		
//	}
//
//
//	
//	@PostMapping("/nuovoprodotto")
//	public String inserisciProdotto(@RequestBody Prodotto objProd) {
//		return objProd.toString();	
//	}
	
	
}
