package com.lezione30.GestioneProdotti.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.lezione30.GestioneProdotti.connessione.ConnettoreDB;
import com.lezione30.GestioneProdotti.model.Prodotto;

public class ProdottoDAO implements Dao<Prodotto>{

	@Override
	public Prodotto getById(int id) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
        
       	String query = "SELECT prodottoid, codice, nome, descrizione FROM prodotto WHERE prodottoid = ?";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setInt(1, id);
       	
       	ResultSet risultato = ps.executeQuery();
       	risultato.next();

   		Prodotto temp = new Prodotto();
   		temp.setProdottoid(risultato.getInt(1));
   		temp.setCodice(risultato.getString(2));
   		temp.setNome(risultato.getString(3));
   		temp.setDescrizione(risultato.getString(4));
       	
       	return temp;
	}

	@Override
	public ArrayList<Prodotto> getAll() throws SQLException {
		ArrayList<Prodotto> elenco = new ArrayList<Prodotto>();
	       
   		Connection conn = ConnettoreDB.getIstanza().getConnessione();
           
       	String query = "SELECT prodottoid, codice, nome, descrizione FROM prodotto ";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	
       	ResultSet risultato = ps.executeQuery();
       	while(risultato.next()){
       		Prodotto temp = new Prodotto();
       		temp.setProdottoid(risultato.getInt(1));
       		temp.setCodice(risultato.getString(2));
       		temp.setNome(risultato.getString(3));
       		temp.setDescrizione(risultato.getString(4));
       		elenco.add(temp);
       	}
       	
       	return elenco;
	}

	@Override
	public void insert(Prodotto t) throws SQLException {
   		Connection conn = ConnettoreDB.getIstanza().getConnessione();

   		String query = "INSERT INTO prodotto (codice, nome, descrizione) VALUES (?,?,?)";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
       	ps.setString(1, t.getCodice());
       	ps.setString(2, t.getNome());
       	ps.setString(3, t.getDescrizione());
       	
       	ps.executeUpdate();
       	ResultSet risultato = ps.getGeneratedKeys();
       	risultato.next();
   		
       	t.setProdottoid(risultato.getInt(1));
		
	}

	@Override
	public boolean delete(Prodotto t) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
   		String query = "DELETE FROM prodotto WHERE prodottoid = ?";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setInt(1, t.getProdottoid());
       	
       	int affRows = ps.executeUpdate();
       	if(affRows > 0)
       		return true;
       	
   		return false;
	}

	@Override
	public Prodotto update(Prodotto t) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
   		String query = "UPDATE prodotto SET "
   				+ "codice = ?, "
   				+ "nome = ?, "
   				+ "descrizione = ? "
   				+ "WHERE prodottoid = ?";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setString(1, t.getCodice());
       	ps.setString(2, t.getNome());
       	ps.setString(3, t.getDescrizione());
       	ps.setInt(4, t.getProdottoid());
       	
       	int affRows = ps.executeUpdate();
       	if(affRows > 0)
       		return getById(t.getProdottoid());
       	
   		return null;
	}

}
