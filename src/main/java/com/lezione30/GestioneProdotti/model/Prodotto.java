package com.lezione30.GestioneProdotti.model;

public class Prodotto {
	
	private Integer prodottoid;
	private String codice;
	private String nome;
	private String descrizione;
	
	public Prodotto() {
		
	}

	public Integer getProdottoid() {
		return prodottoid;
	}
	public void setProdottoid(Integer prodottoid) {
		this.prodottoid = prodottoid;
	}
	public String getCodice() {
		return codice;
	}
	public void setCodice(String codice) {
		this.codice = codice;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescrizione() {
		return descrizione;
	}
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	@Override
	public String toString() {
		return "Prodotto [prodottoid=" + prodottoid + ", codice=" + codice + ", nome=" + nome + ", descrizione="
				+ descrizione + "]";
	}
	
	
	
	
	
}
