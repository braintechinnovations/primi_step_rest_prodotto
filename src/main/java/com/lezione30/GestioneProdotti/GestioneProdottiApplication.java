package com.lezione30.GestioneProdotti;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestioneProdottiApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestioneProdottiApplication.class, args);
	}

	/*
	 * Creare un sistema di gestione di Menu:
	 * Un menu contiene al suo interno prodotti di tipo Piatto o Bevanda
	 * 
	 * Creare un sistema comprensivo di BackEnd e FrontEnd (in AJAX) per effettuare delle CRUD
	 * e permettere (HARD) di filtrare la tabella con tutti i risultati per tipologia di prodotto.
	 */
	
}
